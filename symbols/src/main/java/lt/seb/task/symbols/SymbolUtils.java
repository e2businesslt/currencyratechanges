package lt.seb.task.symbols;

import java.util.ResourceBundle;

public class SymbolUtils {

    public static String getClientSymbol(String key) {
        return getSymbol(Names.CLIENT, key);
    }

    public static String getExceptionSymbol(String key) {
        return getSymbol(Names.EXCEPTION, key);
    }

    private static String getSymbol(String name, String key) {
        String result;
        try {
            ResourceBundle bundle = ResourceBundle.getBundle(name);
            result = bundle.getString(key);
        } catch (Exception e) {
            result = key;
        }
        return (result != null)? result : "";
    }
}
