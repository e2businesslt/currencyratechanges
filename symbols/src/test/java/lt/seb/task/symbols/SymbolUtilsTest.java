package lt.seb.task.symbols;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class SymbolUtilsTest {

    @Test
    public void testWrongKey() {
        try {
            String key = "abc";
            assertEquals(key, SymbolUtils.getClientSymbol(key));
        } catch (Exception e) {
            fail("Exception was thrown. " + e.getMessage());
        }
    }

    @Test
    public void testNullKey() {
        try {
            assertEquals("", SymbolUtils.getExceptionSymbol(null));
        } catch (Exception e) {
            fail("Exception was thrown. " + e.getMessage());
        }
    }

    @Test
    public void testCorrectKey() {
        try {
            assertEquals("utf-8", SymbolUtils.getClientSymbol("charset"));
        } catch (Exception e) {
            fail("Exception was thrown. " + e.getMessage());
        }
    }
}
