# README 
# CurrencyRateChanges

> Make sure you have [Tomcat](http://tomcat.apache.org/download-80.cgi) installed on your local machine.

## Installation

1. Clone the project git clone ```git clone https://e2businesslt@bitbucket.org/e2businesslt/currencyratechanges.git```.
2. Build _**war**_ file by executing ```gradlew :server:build war``` in your command line interface (you must be in project's root folder).
3. Deploy _**CurrencyRatesWs.war**_ file on Tomcat located on _`<project_root>/server/build/libs`_ folder.

## Deploying on Tomcat
1. Execute in your command line:  
  1.1. **```cd <project_root>/server/build/libs```**  
  1.2. **```set CATALINA_HOME="<Tomcat_root>"```**  
  1.3. **```copy CurrencyRatesWs.war %CATALINA_HOME%\webapps```**  
  1.4. **```<Tomcat_root>/bin>catalina.bat stop```**  
  1.5. **```<Tomcat_root>/bin>catalina.bat start```**
  
> Application is running on [http://localhost:9000/exchanges](http://localhost:9000/exchanges)