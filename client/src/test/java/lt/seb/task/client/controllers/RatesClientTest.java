package lt.seb.task.client.controllers;

import lt.seb.task.client.controllers.RatesClient;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class RatesClientTest {

    @Test
    public void testGetCurrencyRates() {
        assertNotNull(new RatesClient().getCurrencyRates("2000.01.01"));
    }
}
