package lt.seb.task.client.controllers;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class CurrencyRatesControllerTest {

    @Test
    public void testGetRates() {
        assertNotNull(new CurrencyRatesController().getCurrencies("2000.01.01").getCurrencyArray()[0].getUnit());
    }
}
