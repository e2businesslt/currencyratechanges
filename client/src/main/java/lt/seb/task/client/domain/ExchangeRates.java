package lt.seb.task.client.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Arrays;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ExchangeRates")
public class ExchangeRates {

    @XmlElement(name = "item")
    private Currency[] currencyArray;

    public Currency[] getCurrencyArray() {
        return currencyArray;
    }

    public void setCurrencyArray(Currency[] currencyArray) {
        this.currencyArray = currencyArray;
    }

    @Override
    public String toString() {
        return "ExchangeRates{" +
                "currencyArray=" + Arrays.toString(currencyArray) +
                "}";
    }
}
