package lt.seb.task.client.controllers;

import lt.seb.task.client.ConnectionUtils;
import lt.seb.task.client.exceptions.DataSendException;
import lt.seb.task.symbols.SymbolUtils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class RatesClient {

    public String getCurrencyRates(String date) {
        HttpURLConnection connection = ConnectionUtils.createConnection();
        String params = SymbolUtils.getClientSymbol("key") + "=" + date;
        byte[] data = params.getBytes();
        connection.setRequestProperty("Content-Length", data.length + "");
        sendRequest(connection, data);
        return getResponse(connection);
    }

    private void sendRequest(HttpURLConnection connection, byte[] data) {
        try {
            DataOutputStream output = new DataOutputStream(connection.getOutputStream());
            output.write(data);
            output.flush();
        } catch (Exception e) {
            throw new DataSendException(SymbolUtils.getExceptionSymbol("send_fail") + e.getMessage());
        }
    }

    private String getResponse(HttpURLConnection connection) {
        StringBuilder response = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line).append("\n");
            }
        } catch (Exception e) {
            throw new DataSendException(SymbolUtils.getExceptionSymbol("get_fail") + e.getMessage());
        }
        return response.toString();
    }
}
