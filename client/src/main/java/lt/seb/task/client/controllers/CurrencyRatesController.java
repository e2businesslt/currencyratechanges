package lt.seb.task.client.controllers;

import lt.seb.task.client.domain.ExchangeRates;

import javax.xml.bind.JAXB;
import java.io.StringReader;

public class CurrencyRatesController {

    public ExchangeRates getCurrencies(String date) {
        String xmlResponse = new RatesClient().getCurrencyRates(date);
        return JAXB.unmarshal(new StringReader(xmlResponse), ExchangeRates.class);
    }
}
