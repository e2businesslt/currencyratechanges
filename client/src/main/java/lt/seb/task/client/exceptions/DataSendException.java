package lt.seb.task.client.exceptions;

public class DataSendException extends RuntimeException {

    public DataSendException(String message) {
        super(message);
    }
}
