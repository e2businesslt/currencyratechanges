package lt.seb.task.client.domain;

import javax.xml.bind.annotation.*;
import java.time.LocalDate;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "item", propOrder = {
        "date",
        "currency",
        "quantity",
        "rate",
        "unit"
})
public class Currency implements Comparable<Currency> {

    @XmlElement(name = "date")
    private String date;
    @XmlElement(name = "currency")
    private String currency;
    @XmlElement(name = "quantity")
    private Integer quantity;
    @XmlElement(name = "rate")
    private Double rate;
    @XmlElement(name = "unit")
    private String unit;


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "date=" + date + ", " +
                "currency=" + currency + ", " +
                "quantity=" + quantity + ", " +
                "rate=" + rate + ", " +
                "unit=" + unit + ", " +
                "}";
    }

    @Override
    public int compareTo(Currency currency) {
        if (rate < currency.getRate()) return -1;
        if (rate > currency.getRate()) return 1;
        return 0;
    }
}
