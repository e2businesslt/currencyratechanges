package lt.seb.task.client;

import lt.seb.task.client.exceptions.NoConnectionException;
import lt.seb.task.symbols.SymbolUtils;

import java.net.HttpURLConnection;
import java.net.URL;

public class ConnectionUtils {

    public static HttpURLConnection createConnection() {
        HttpURLConnection connection;
        try {
            URL url = new URL(SymbolUtils.getClientSymbol("host"));
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", SymbolUtils.getClientSymbol("content_type"));
            connection.setRequestProperty("charset", SymbolUtils.getClientSymbol("charset"));
        } catch (Exception e) {
            throw new NoConnectionException(SymbolUtils.getExceptionSymbol("no_connection") + e.getMessage());
        }
        return connection;
    }
}
