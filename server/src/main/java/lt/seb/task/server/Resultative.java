package lt.seb.task.server;

public interface Resultative {

    Integer RESULT_OK = 200;
    Integer RESULT_ERROR = 400;
    Integer RESULT_INTERNAL_ERROR = 500;

    void setResultCode(Integer resultCode);
    Integer getResultCode();
    void setDescription(String description);
    String getDescription();
}
