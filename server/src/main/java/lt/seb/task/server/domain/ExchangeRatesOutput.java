package lt.seb.task.server.domain;

import lt.seb.task.client.domain.ExchangeRates;
import lt.seb.task.server.Resultative;

public class ExchangeRatesOutput implements Resultative {

    private Integer resultCode;
    private String description;
    private ExchangeRates rates;

    public ExchangeRatesOutput() {
    }

    public ExchangeRatesOutput(Integer resultCode, String description) {
        this.resultCode = resultCode;
        this.description = description;
    }

    public ExchangeRatesOutput(Integer resultCode, String description, ExchangeRates rates) {
        this.resultCode = resultCode;
        this.description = description;
        this.rates = rates;
    }

    @Override
    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    @Override
    public Integer getResultCode() {
        return resultCode;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public ExchangeRates getRates() {
        return rates;
    }

    public void setRates(ExchangeRates rates) {
        this.rates = rates;
    }

    @Override
    public String toString() {
        return "ExchangeRatesOutput{" +
                "resultCode=" + resultCode + ", " +
                "description=" + description + ", " +
                "exchangeRates=" + rates.toString() + ", " +
                "}";
    }
}
