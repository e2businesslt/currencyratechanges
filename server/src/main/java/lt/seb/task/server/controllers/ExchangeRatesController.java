package lt.seb.task.server.controllers;

import lt.seb.task.client.controllers.CurrencyRatesController;
import lt.seb.task.client.domain.ExchangeRates;
import lt.seb.task.server.Resultative;
import lt.seb.task.server.domain.ExchangeRatesInput;
import lt.seb.task.server.domain.ExchangeRatesOutput;
import lt.seb.task.worker.TextManager;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Currency;

@RestController
@RequestMapping("/exchanges")
public class ExchangeRatesController {

    @RequestMapping(method = RequestMethod.POST)
    public ExchangeRatesOutput downloadExchangeRates(@RequestBody ExchangeRatesInput input) {
        try {
            TextManager manager = new TextManager();
            String date = manager.convertDate(input.getDate());
            ExchangeRates rates = new CurrencyRatesController().getCurrencies(date);
            manager.sortCurrencies(rates.getCurrencyArray());
            return new ExchangeRatesOutput(Resultative.RESULT_OK, "Success", rates);
        } catch (Exception e) {
            return new ExchangeRatesOutput(Resultative.RESULT_ERROR, e.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public ExchangeRatesOutput downloadExchangeRates() {
        return new ExchangeRatesOutput(Resultative.RESULT_OK, "Server is running");
    }
}
