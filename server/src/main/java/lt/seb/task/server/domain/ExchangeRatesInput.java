package lt.seb.task.server.domain;

public class ExchangeRatesInput {

    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "ExchangeRatesInput{" +
                "date=" + date +
                "}";
    }
}
