package lt.seb.task.server;

import lt.seb.task.server.controllers.ExchangeRatesController;
import lt.seb.task.server.domain.ExchangeRatesInput;
import lt.seb.task.symbols.SymbolUtils;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ExchangeRatesControllerTest {

    private static ExchangeRatesInput input;
    private static ExchangeRatesController controller;

    @BeforeClass
    public static void setUpClass() {
        input = new ExchangeRatesInput();
        controller = new ExchangeRatesController();
    }

    @Ignore
    @Test
    public void testPostMethod() {
        input.setDate("2000-01-01");
        assertNotNull(controller.downloadExchangeRates(input).getRates().getCurrencyArray());
    }

    @Test
    public void testPostWithLongDate() {
        input.setDate("20000/01/01");
        assertEquals(SymbolUtils.getExceptionSymbol("date_length"), getOutputDescription(input));
    }

    @Test
    public void testPostWrongDateSeparator() {
        input.setDate("2000*01*01");
        assertEquals(SymbolUtils.getExceptionSymbol("date_separator"), getOutputDescription(input));
    }

    @Test
    public void testPostDateWithLetters() {
        input.setDate("878-sd.4568");
        assertEquals(SymbolUtils.getExceptionSymbol("date_letters"), getOutputDescription(input));
    }

    @Test
    public void testPostLongDate() {
        input.setDate("2000.01.010");
        assertEquals(SymbolUtils.getExceptionSymbol("date_length"), getOutputDescription(input));
    }

    @Test
    public void testPostShortDate() {
        input.setDate("200.1.1");
        assertEquals(SymbolUtils.getExceptionSymbol("date_length"), getOutputDescription(input));
    }

    @Test
    public void testPostMoreSeparators() {
        input.setDate("2000.1.1.1");
        assertEquals(SymbolUtils.getExceptionSymbol("date_format"), getOutputDescription(input));
    }

    @Test
    public void testPostYearsBeforeRange() {
        input.setDate("1999.01.01");
        assertEquals(SymbolUtils.getExceptionSymbol("date_between"), getOutputDescription(input));
    }

    @Test
    public void testPostYearsAfterRange() {
        input.setDate("2015.01.01");
        assertEquals(SymbolUtils.getExceptionSymbol("date_between"), getOutputDescription(input));
    }

    @Test
    public void testPostMonthsOutOfRange() {
        input.setDate("2000.13.01");
        assertEquals(SymbolUtils.getExceptionSymbol("date_between"), getOutputDescription(input));
    }

    @Test
    public void testPostDaysOutOfRange() {
        input.setDate("2000.01.32");
        assertEquals(SymbolUtils.getExceptionSymbol("date_between"), getOutputDescription(input));
    }

    @Test
    public void testPostNullDate() {
        input.setDate(null);
        assertEquals(SymbolUtils.getExceptionSymbol("date_not_set"), getOutputDescription(input));
    }

    @Test
    public void testPostEmptyDate() {
        input.setDate(" ");
        assertEquals(SymbolUtils.getExceptionSymbol("date_not_set"), getOutputDescription(input));
    }

    @Test
    public void testGetMethod() {
        assertEquals(Resultative.RESULT_OK, controller.downloadExchangeRates().getResultCode());
    }

    private String getOutputDescription(ExchangeRatesInput input) {
        return controller.downloadExchangeRates(input).getDescription();
    }
}
