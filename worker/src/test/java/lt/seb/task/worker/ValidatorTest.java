package lt.seb.task.worker;

import lt.seb.task.symbols.SymbolUtils;
import lt.seb.task.worker.exceptions.InvalidDateFormatException;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertTrue;

public class ValidatorTest {

    private static Validator validator;
    @Rule
    public ExpectedException expected = ExpectedException.none();


    @BeforeClass
    public static void setUpClass() {
        validator = new Validator();
    }

    @Test
    public void testNoLetters() {
        assertTrue(validator.isDateNotContainsLetters("2011.05.05"));
    }

    @Test
    public void testWithLetters() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_letters"));
        validator.isDateNotContainsLetters("5113.fgb4568");
    }


    @Test
    public void testLongerDateLength() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_length"));
        validator.isDateLengthValid("2000.01.011");
    }

    @Test
    public void testShorterDateLength() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_length"));
        validator.isDateLengthValid("200.1.1");
    }

    @Test
    public void testDateLength() {
        assertTrue(validator.isDateLengthValid("2000.1.1") && validator.isDateLengthValid("2000.01.01"));
    }

    @Test
    public void testOneSeparator() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_format"));
        validator.isDateFormatValid("2000.1");
    }

    @Test
    public void testThreeSeparators() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_format"));
        validator.isDateFormatValid("2000.1.1.1");
    }

    @Test
    public void testEmptyYears() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_format"));
        validator.isDateFormatValid(".01.01");
    }

    @Test
    public void testEmptyMonth() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_format"));
        validator.isDateFormatValid("2000..1");
    }

    @Test
    public void testEmptyDays() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_format"));
        validator.isDateFormatValid("2000.01.");
    }

    @Test
    public void testShortYears() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_format"));
        validator.isDateFormatValid("200.01.1");
    }

    @Test
    public void testLongerYears() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_format"));
        validator.isDateFormatValid("20000.1.1");
    }

    @Test
    public void testLongerMonths() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_format"));
        validator.isDateFormatValid("2000.001.1");
    }

    @Test
    public void testLongerDays() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_format"));
        validator.isDateFormatValid("2000.01.001");
    }

    @Test
    public void testTooLowYears() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_between"));
        validator.isDateFormatValid("1999.01.01");
    }

    @Test
    public void testTooHighYears() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_between"));
        validator.isDateFormatValid("2015.01.01");
    }

    @Test
    public void testTooLowMonths() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_between"));
        validator.isDateFormatValid("2000.-1.01");
    }

    @Test
    public void testTooHighMonths() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_between"));
        validator.isDateFormatValid("2000.13.01");
    }

    @Test
    public void testTooLowDays() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_between"));
        validator.isDateFormatValid("2000.01.-1");
    }

    @Test
    public void testTooHighDays() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_between"));
        validator.isDateFormatValid("2000.01.32");
    }

    @Test
    public void testValidFormatFirst() {
        assertTrue(validator.isDateFormatValid("2000.1.1"));
    }

    @Test
    public void testValidFormatSecond() {
        assertTrue(validator.isDateFormatValid("2000.01.01"));
    }
}