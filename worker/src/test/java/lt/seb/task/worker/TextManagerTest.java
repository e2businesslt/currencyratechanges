package lt.seb.task.worker;

import lt.seb.task.client.domain.Currency;
import lt.seb.task.symbols.SymbolUtils;
import lt.seb.task.worker.exceptions.InvalidDateFormatException;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TextManagerTest {

    private static TextManager manager;

    @Rule
    public ExpectedException expected = ExpectedException.none();

    @BeforeClass
    public static void setUpClass() {
        manager = new TextManager();
    }

    @Test
    public void testWrongDateSeparator() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_separator"));
        manager.convertDate("2000*01*01");
    }

    @Test
    public void testWithLetters() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_letters"));
        manager.convertDate("5113.fgb4568");
    }


    @Test
    public void testLongerDateLength() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_length"));
        manager.convertDate("2000.01.011");
    }

    @Test
    public void testShorterDateLength() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_length"));
        manager.convertDate("200.1.1");
    }

    @Test
    public void testThreeSeparators() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_format"));
        manager.convertDate("2000.1.1.1");
    }

    @Test
    public void testEmptyDays() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_format"));
        manager.convertDate("2000.01.");
    }

    @Test
    public void testShortYears() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_format"));
        manager.convertDate("200.01.1");
    }

    @Test
    public void testLongerYears() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_format"));
        manager.convertDate("20000.1.1");
    }

    @Test
    public void testLongerMonths() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_format"));
        manager.convertDate("2000.001.1");
    }

    @Test
    public void testTooLowYears() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_between"));
        manager.convertDate("1999.01.01");
    }

    @Test
    public void testTooHighYears() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_between"));
        manager.convertDate("2015.01.01");
    }

    @Test
    public void testTooHighMonths() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_between"));
        manager.convertDate("2000.13.01");
    }

    @Test
    public void testTooHighDays() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_between"));
        manager.convertDate("2000.01.32");
    }

    @Test
    public void testNullDate() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_not_set"));
        manager.convertDate(null);
    }

    @Test
    public void testEmptyDate() {
        expected.expect(InvalidDateFormatException.class);
        expected.expectMessage(SymbolUtils.getExceptionSymbol("date_not_set"));
        manager.convertDate(" ");
    }

    @Test
    public void testConvert() {
        assertEquals("2000.01.01", manager.convertDate("2000/01/01"));
    }

    @Test
    public void testSorting() {
        Currency[] currencies = new Currency[4];
        currencies[0] = getCurrency(0.51);
        currencies[1] = getCurrency(1.5);
        currencies[2] = getCurrency(0.81);
        currencies[3] = getCurrency(2.544);
        manager.sortCurrencies(currencies);
        assertTrue(currencies[0].getRate().equals(2.544) && currencies[3].getRate().equals(0.51));
    }

    private Currency getCurrency(Double rate) {
        Currency currency = new Currency();
        currency.setRate(rate);
        return currency;
    }
}
