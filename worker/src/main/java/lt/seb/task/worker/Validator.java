package lt.seb.task.worker;

import lt.seb.task.symbols.SymbolUtils;
import lt.seb.task.worker.exceptions.InvalidDateFormatException;

public class Validator {

    public boolean isDateNotContainsLetters(String date) {
        if (date.matches("[\\d\\W]*"))
            return true;
        else
            throw new InvalidDateFormatException(SymbolUtils.getExceptionSymbol("date_letters"));
    }

    public boolean isDateLengthValid(String date) {
        if (date.length() < 11 && date.length() > 7) {
            return true;
        } else {
            throw new InvalidDateFormatException(SymbolUtils.getExceptionSymbol("date_length"));
        }
    }

    public boolean isDateFormatValid(String date) {
        String[] parts = date.split("\\.");
        return isFormatValid(parts) && isDateValid(parts);
    }

    private boolean isFormatValid(String[] parts) {
        if (parts.length == 3 &&
                parts[0].length() == 4 &&
                parts[1].length() > 0 &&
                parts[2].length() > 0 &&
                parts[1].length() < 3 &&
                parts[2].length() < 3) {
            return true;
        } else {
            throw new InvalidDateFormatException(SymbolUtils.getExceptionSymbol("date_format"));
        }
    }

    private boolean isDateValid(String[] parts) {
        int years = Integer.parseInt(parts[0]);
        int months = Integer.parseInt(parts[1]);
        int days = Integer.parseInt(parts[2]);
        if (years > 1999 && years < 2015 && months > 0 && months < 13 && days > 0 && days < 32) {
            return true;
        }
        throw new InvalidDateFormatException(SymbolUtils.getExceptionSymbol("date_between"));
    }
}
