package lt.seb.task.worker;

import lt.seb.task.client.domain.Currency;
import lt.seb.task.symbols.SymbolUtils;
import lt.seb.task.worker.exceptions.InvalidDateFormatException;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collector;

public class TextManager {

    public String convertDate(String date) {
        Validator validator = new Validator();

        if (date == null || date.trim().isEmpty()) {
            throw new InvalidDateFormatException(SymbolUtils.getExceptionSymbol("date_not_set"));
        }

        if (validator.isDateNotContainsLetters(date) && validator.isDateLengthValid(date)) {
            date = convert(date);
            validator.isDateFormatValid(date);
        }
        return date;
    }

    private String convert(String date) {
        if (date.contains("/")) {
            date = date.replace("/", ".");
        } else if (date.contains("\\")) {
            date = date.replace("\\", ".");
        } else if (date.contains("-")) {
            date = date.replace("-", ".");
        } else if (date.contains(" ")) {
            date = date.replace(" ", ".");
        } else if (date.contains(",")) {
            date = date.replace(",", ".");
        } else if (date.contains(".")) {
            return date;
        } else {
            throw new InvalidDateFormatException(SymbolUtils.getExceptionSymbol("date_separator"));
        }
        return date;
    }

    public void sortCurrencies(Currency[] currencies) {
        if (currencies != null && currencies.length > 0) {
            Arrays.asList(currencies)
                    .sort(Collections
                            .reverseOrder(Comparator
                                    .comparing(Currency::getRate)));
        }
    }
}
